package com.estangi.utils.toolkit;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MiscellaneousTest {

    @Test
    void oneNull() {

        assertTrue(Miscellaneous.oneNull(1, 2, null));
        assertFalse(Miscellaneous.oneNull(1, 2, 3));

    }

    @Test
    void oneOfIsNotEmpty() {

        assertFalse(Miscellaneous.oneOfIsNotEmpty(emptyList(), emptySet()));
        assertTrue(Miscellaneous.oneOfIsNotEmpty(emptyList(), emptySet(), Lists.newArrayList(1, 2, 3)));

    }
}