package com.estangi.utils.toolkit;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static com.estangi.utils.toolkit.Strings.EMPTY;
import static com.estangi.utils.toolkit.TestConstant.TEST_STRING;
import static org.junit.jupiter.api.Assertions.*;

class StringsTest {

    @Test
    void optional() {

        final Optional<String> testOptional = Strings.optional(TEST_STRING);

        assertFalse(Strings.optional(null).isPresent());
        assertFalse(Strings.optional(EMPTY).isPresent());
        assertTrue(testOptional.isPresent());
        assertEquals(testOptional.get(), TEST_STRING);
    }

    @Test
    void emptyToNull() {
        assertEquals(Strings.emptyToNull(TEST_STRING), TEST_STRING);
        assertNull(Strings.emptyToNull(EMPTY));
    }
}
