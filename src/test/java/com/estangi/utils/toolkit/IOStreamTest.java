package com.estangi.utils.toolkit;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static com.estangi.utils.toolkit.Strings.EMPTY;
import static com.estangi.utils.toolkit.TestConstant.TEST_STRING;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class IOStreamTest {

    @Test
    void toInputStream() throws IOException {

        final InputStream fromNullInputStream = IOStream.toInputStream(null);
        final InputStream fromEmptyInputStream = IOStream.toInputStream(EMPTY);
        final InputStream testInputStream = IOStream.toInputStream(TEST_STRING);

        assertNull(inputStreamToString(fromNullInputStream));
        assertNull(inputStreamToString(fromEmptyInputStream));
        assertEquals(inputStreamToString(testInputStream), TEST_STRING);

    }

    private static String inputStreamToString(InputStream inputStream) throws IOException {
        final BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String str = br.readLine();
        br.close();
        return str;
    }
}