package com.estangi.utils.toolkit;

import java.util.Collection;
import java.util.UUID;

/**
 * Class name should not be more explicit on what inside the class
 */
class Miscellaneous {

    /**
     * Return a pseudo unique identifier
     * @return String : identifier
     */
    public static String generateID() {
        return UUID.randomUUID().toString();
    }

    /**
     * Check whether or not one object is null on those pass as parameter
     * @param obj list of oibject to check
     * @return true if there is a null object
     */
    public static boolean oneNull(Object...obj) {
        for(Object o:obj){
            if(o == null)return true;
        }
        return false;
    }


    /**
     * Check if a collections is not empty
     * @param collections list of collection
     * @return true if a collection is not empty
     */
    public static boolean oneOfIsNotEmpty(Collection...collections){
        for (Collection c:collections){
            if(!c.isEmpty())return true;
        }
        return false;
    }

}
