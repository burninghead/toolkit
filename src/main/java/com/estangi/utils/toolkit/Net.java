package com.estangi.utils.toolkit;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * An utility class used do deal with Net service
 */
public class Net {

    public static String URLtoString(URL url) throws IOException {
        return new Net().toString(url);
    }

    private String toString(URL url) throws IOException {
        final URLConnection connection = url.openConnection();
        connection.setConnectTimeout(10000);
        connection.setReadTimeout(10000);
        final InputStream inputStream = connection.getInputStream();
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream(10000);

        IOUtils.copy(inputStream, outputStream);

        final String content = outputStream.toString(UTF_8.name());

        return content;
    }


}
