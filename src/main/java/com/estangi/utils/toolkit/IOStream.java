package com.estangi.utils.toolkit;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Optional;

import static com.estangi.utils.toolkit.Strings.nullToEmpty;

/**
 * An utility class with convenient method to handle IO Stream
 */
class IOStream {

    /**
     * Turn String pass as parameter into an InputStream
     * @param str a String
     * @return
     */
    public static InputStream toInputStream(String str) {
        return new ByteArrayInputStream(nullToEmpty(str).getBytes());
    }

}
