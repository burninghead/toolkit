package com.estangi.utils.toolkit;

import java.util.Optional;

/**
 * Some useful methods to handle with String
 */
class Strings {

    public static final String EMPTY  = "";
    public static final String SPACE  = " ";

    /**
     * Create an Optional class with String pass as parameter wrapped.
     * If the String parameter is empty, Optional return will be empty, ie return false to Optional.isPresent() method.
     * @param str, String which will be wrapped in the optional.
     * @return an Optional with String wrapped which won't be present if String is empty.
     */
    public static Optional<String> optional(String str){
        return Optional.ofNullable(emptyToNull(str));
    }

    /**
     * return null value if String pass as parameter is null or is empty.
     * @param str, String to check.
     * @return string parameter if it is not null or an empty String, null otherwise.
     */
    public static String emptyToNull(String str){
        return str != null && str.length() == 0 ? null : str;
    }

    /**
     * return an empty String if the one pass as parameter is null.
     * @param str, String to check.
     * @return an empty String if parameter is null, String itself otherwise.
     */
    public static String nullToEmpty(String str){
        return str == null ? EMPTY : str;
    }
}
