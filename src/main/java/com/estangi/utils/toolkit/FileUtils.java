package com.estangi.utils.toolkit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;

import static com.estangi.utils.toolkit.Miscellaneous.generateID;
import static java.lang.System.lineSeparator;
import static java.util.Optional.empty;

/**
 * Class utilities to read / write files more easily
 */
public class FileUtils {

    /**
     * Check whether or not a file exists and is not a directory
     * @param filePath
     * @return true if file exists and is not a directory
     */
    public static boolean fileExists(String filePath) {
        final File file = new File(filePath);
        return file.exists() && !file.isDirectory();
    }

    public static Optional<String> fileToString(String filePath) throws IOException {
        if(fileExists(filePath)){
            final StringBuilder contentBuilder = new StringBuilder();
            final BufferedReader br = new BufferedReader(new FileReader(filePath));
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null)
            {
                contentBuilder.append(sCurrentLine).append(lineSeparator());
            }
            return Optional.of(contentBuilder.toString());
        }
        return Optional.empty();
    }

    public static Optional<String> readFileFromPath(String pathFileName) {
        try {
            return fileToString(pathFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return empty();
    }

    /**
     * Return and create a directory if it is not already existing
     *
     * @param path
     * @return File
     */
    public static File createDirectory( final String path){
        File f = new File(path);
        if(!f.exists()){
            f.mkdirs();
        }
        return f;
    }

    /**
     * Create a temporary file
     *
     * @return
     * @throws IOException
     */
    public static File createTempFile() throws IOException {
        return File.createTempFile(generateID(), ".tmp");
    }

}
