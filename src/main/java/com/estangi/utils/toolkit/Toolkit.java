package com.estangi.utils.toolkit;

/**
 * Entry Point from this utility project
 * All features can be found there
 * <p>
 * This should be the case with all version of the project
 */
public final class Toolkit {

    private Toolkit() {
    }

    public static final class Strings extends com.estangi.utils.toolkit.Strings {
        private Strings() {
        }
    }

    public static final class Miscellaneous extends com.estangi.utils.toolkit.Miscellaneous {
        private Miscellaneous() {
        }
    }

    public static final class IOStream extends com.estangi.utils.toolkit.IOStream {
        private IOStream() {
        }
    }

    public static final class FileUtils extends com.estangi.utils.toolkit.FileUtils {
        private FileUtils() {
        }
    }




}
